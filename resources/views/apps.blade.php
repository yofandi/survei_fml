<!doctype html>
<html lang="en">


<!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/analytics-variation.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 12:04:37 GMT -->
<!-- Added by HTTrack -->
<head>
    @include('layout/css')
    @yield('title')
</head>
<body id="app">
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        @include('layout/appbar')

        <div class="app-main">
            @include('layout/sidebar')         
            <div class="app-main__outer">
                <div class="app-main__inner">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <div class="app-drawer-overlay d-none animated fadeIn"></div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    @include('layout/js')
    @yield('js')
</body>

<!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/analytics-variation.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 12:04:37 GMT -->
</html>
