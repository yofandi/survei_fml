<script>
	$('#grid_wilayah').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: '/api/wilayah/get',
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: '/api/wilayah/add',
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: '/api/wilayah/edit',
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: '/api/wilayah/delete',
					data: item
				});
			},
		},

		fields: [
		{
			name: "id",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "nama_wil",
			title: "NAMA WILAYAH",
			type: "text", 
			width: 50
		},
		{
			type: "control"
		}
		]
	});
	
	function Refresh() {
		$("#grid_wilayah").jsGrid("loadData");
	}
</script>