<script>
	let token = $("meta[name='csrf-token']").attr("content");

	function tampilkanPreview(gambar,idpreview){
		var gb = gambar.files;
		for (var i = 0; i < gb.length; i++){
			var gbPreview = gb[i];
			var imageType = /image.*/;
			var preview=document.getElementById(idpreview);            
			var reader = new FileReader();

			if (gbPreview.type.match(imageType)) {
				preview.file = gbPreview;
				reader.onload = (function(element) { 
					return function(e) { 
						element.src = e.target.result; 
					}; 
				})(preview);
				reader.readAsDataURL(gbPreview);
			}else{
				alert("file yang anda upload tidak sesuai. Khusus mengunakan image.");
			}

		}    
	}

	var table = $('#table-kegiatan').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/kegiatan/get',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_keg', name: 'tanggal_keg' },
		{ data: 'nama_cell', name: 'nama_cell' },
		{ data: 'jml_dewasa', name: 'jml_dewasa' },
		{ data: 'jml_anak', name: 'jml_anak' },
		{ data: 'jml_jemaatbaru', name: 'jml_jemaatbaru' },
		{ data: 'total', name: 'total' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$button = '<button type="button" class="btn btn-xs btn-rounded btn-primary foto-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += 'data-name="'+ o.tanggal_keg +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-xs btn-rounded btn-warning update-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += 'data-id_wilayah="'+ o.tb_wilayah_id +'"';
				$button += 'data-name="'+ o.tanggal_keg +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += '><i class="fa fa-trash"></i></button>';
				return $button; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-kegiatan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-kegiatan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function loader() {
		$gert = '<div class="loader-wrapper d-flex justify-content-center align-items-center">';
		$gert += '<div class="loader" align="center">';
		$gert += '<div class="ball-spin-fade-loader">';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '</div>';
		$gert += '</div>';
		$gert += '</div>';

		return $gert;
	}

	function alert(jenis, point, text) {
		$art = '<div class="alert alert-'+ jenis +' alert-dismissible fade show" role="alert">';
		$art += '<strong>'+ point +'</strong> ' + text;
		$art += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
		$art += '<span aria-hidden="true">&times;</span>';
		$art += '</button>';
		$art += '</div>';

		return $art;
	}

	$('#tanggal').datepicker({
		format: 'yyyy-mm-dd',
	});

	$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
		$(".alert-dismissible").slideUp(500);
	});

	function Refresh() {
		$('#table-kegiatan').DataTable().ajax.reload();
	}

	function select_wilayah(like = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/wilayah/get',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			$html = '<option value="default">--- Pilih ----</option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'"';
				if (like == data[i].id) {
					$html += ' selected';
				}
				$html += '>'+ data[i].nama_wil +'</option>';
			}
			$('#wilayah').html($html);
		});
	}

	function select_familycell(basedon, like = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/familycell/getlist',
			type: 'POST',
			dataType: 'json',
			data: { wilid: basedon }
		})
		.done(function(data) {
			$html = '<option value="default">--- Pilih ----</option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'"';
				if (like == data[i].id) {
					$html += ' selected';
				}
				$html += '>'+ data[i].nama_cell +'</option>';
			}
			$('#family_cell').html($html);
		});	
	}

	$(document).on('change', '#wilayah', function(event) {
		let pure = $(this).val();
		select_familycell(pure);
	});

	$(document).on('click', '.add-data', function(event) {
		let frt = $(this).data('title');

		$('#divpass').fadeIn();

		select_wilayah();

		$('#form-data').attr('action', '/api/kegiatan/add');
		$('#form-data')[0].reset();
		$('#exampleModalLongTitle').text(frt);
	});

	$(document).on('click', '.update-row', function(event) {
		let kare = $(this);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/kegiatan/getRow',
			type: 'POST',
			dataType: 'json',
			data:  { id: kare.data('id') }
		})
		.done(function(data) {
			$('#divpass').hide();
			$('#form-data')[0].reset();

			$('#tanggal').val(data.tanggal_keg);
			$('#lokasi').val(data.lokasi_keg);
			$('#jml_dws').val(data.jml_dewasa);
			$('#jml_ank').val(data.jml_anak);
			$('#jml_jmtbr').val(data.jml_jemaatbaru);
			$('#total').val(data.total);

			select_wilayah(kare.data('id_wilayah'));
			select_familycell(kare.data('id_wilayah'),data.tb_familycell_id);

			$('#form-data').attr('action', '/api/kegiatan/upp/'+ data.id);
			$('#exampleModalLongTitle').text('Update Kegiatan - '+ kare.data('name'));
			$('.bd-example-modal-lg').modal('toggle');
		});
		
	});

	$(document).on('click', '.foto-row', function(event) {
		let kare = $(this);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/kegiatan/getRow',
			type: 'POST',
			dataType: 'json',
			data:  { id: kare.data('id') }
		})
		.done(function(data) {
			$('#form-datafoto')[0].reset();

			// $('#tanggal').val(data.tanggal_keg);
			$('#preview').attr('src', '/images/kegiatan/' + data.foto_kegiatan);

			$('#form-datafoto').attr('action', '/api/kegiatan/postfoto/'+ data.id);
			$('#exampleModalLongTitle1').text('Foto Kegiatan - '+ kare.data('name'));
			$('.bd-example1-modal-lg').modal('toggle');
		});
		
	});

	$(document).on('click', '#simpan-data', function(event) {
		var form = $("#form-data");

		var formData = new FormData(form[0]);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $(form).prop("action"),
			type: 'POST',
			dataType: 'json',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-hidden').html(erp);

				$('#table-kegiatan').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-hidden').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-hidden').html(nert);

			$('#form-data')[0].reset();
			$('#table-kegiatan').DataTable().ajax.reload();
		});
	});

	$(document).on('click', '#simpan-datafoto', function(event) {
		var form = $("#form-datafoto");

		var formData = new FormData(form[0]);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $(form).prop("action"),
			type: 'POST',
			async: true,
			dataType: 'json',
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-hidden-foto').html(erp);

				$('#table-kegiatan').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-hidden-foto').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-hidden-foto').html(nert);

			$('#form-datafoto')[0].reset();
			$('#table-kegiatan').DataTable().ajax.reload();
		});
	});

	$(document).on('click', '#delete-row', function(event) {
		let crt = $(this).data('id');
		let jey = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jey) {
			$.ajax({
				headers: {
					'X-CSRF-Token': token 
				},
				url: '/api/kegiatan/delete',
				type: 'POST',
				dataType: 'json',
				data: {
					id: crt
				},
				error: function(xhr, data) { 
					alert(xhr.statusText + xhr.responseText);
					$jenis = 'danger';
					$point = 'Gagal';
					let erp = alert($jenis, $point, xhr.statusText);
					$('#loader-table').html(erp);
					$('#table-family').DataTable().ajax.reload();
				},
			})
			.done(function(data) {
				$jenis = 'success';
				$point = 'Berhasil';
				let nert = alert($jenis, $point, data);
				$('#loader-table').html(nert);
				$('#table-kegiatan').DataTable().ajax.reload();
			});
		}
	});
</script>