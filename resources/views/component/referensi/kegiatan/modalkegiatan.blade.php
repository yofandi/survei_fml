<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="loader-hidden">
                    </div>
                </div>
                <form id="form-data" method="POST">
                    <div class="modal-body">
                        {{-- <h5 class="card-title">Grid Rows</h5> --}}
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="tanggal" class="">Tanggal</label>
                                    <input name="tanggal" id="tanggal" placeholder="00-00-0000" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="wilayah" class="">wilayah</label>
                                    <select name="wilayah" id="wilayah" class="form-control" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label for="family_cell" class="">Family Cell</label>
                                    <select name="family_cell" id="family_cell" class="form-control" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="lokasi" class="">Lokasi</label>
                                    <textarea name="lokasi" id="lokasi" placeholder="Lokasi" type="text" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="jml_dws" class="">Jumlah Dewasa</label>
                                    <input name="jml_dws" id="jml_dws" placeholder="" type="number" class="form-control"  required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="jml_ank" class="">Jumlah Anak</label>
                                    <input name="jml_ank" id="jml_ank" placeholder="" type="number" class="form-control"  required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="jml_jmtbr" class="">Jumlah Jemaat Baru</label>
                                    <input name="jml_jmtbr" id="jml_jmtbr" placeholder="" type="number" class="form-control"  required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="total" class="">Total</label>
                                    <input name="total" id="total" placeholder="" type="number" class="form-control"  required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group" id="divpass">
                            <label for="alamat" class="">Foto</label>
                            <input name="image" id="image" type="file" class="form-control-file">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="simpan-data" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle1"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="loader-hidden-foto">
                    </div>
                </div>
                <form id="form-datafoto" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <h5 class="card-title">Foto</h5>
                        <div class="form-row">
                            {{-- <label for="alamat" class="">Foto</label> --}}
                            <div class="col-md-12" align="center">
                                <img id="preview" height="300" width="450" class="img-circle" alt="User Image"/>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <input accept="image/*" onchange="tampilkanPreview(this,'preview')" type="file" name="image" id="image" class="form-control-file">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="simpan-datafoto" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>