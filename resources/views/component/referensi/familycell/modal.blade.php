<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="loader-hidden">
                    </div>
                </div>
                <form id="form-data" method="POST">
                    <div class="modal-body">
                        {{-- <h5 class="card-title">Grid Rows</h5> --}}
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="nama" class="">Nama</label>
                                    <input name="nama" id="nama" placeholder="Nama Wilayah" type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="telefon" class="">Telefon</label>
                                    <input name="telefon" id="telefon" placeholder="08######" type="text" class="form-control"  required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <label for="wilayah" class="">Wilayah</label>
                            <select name="wilayah" id="wilayah" class="form-control" required>
                            </select>
                        </div>
                        <div class="position-relative form-group">
                            <label for="alamat" class="">Alamat</label>
                            <textarea name="alamat" id="alamat" placeholder="............." class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="simpan-data" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle1"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="loader-hidden-leader">
                    </div>
                </div>
                <form id="form-dataleader" method="POST">
                    <div class="modal-body">
                        {{-- <h5 class="card-title">Grid Rows</h5> --}}
                        <div class="position-relative form-group">
                            <label for="leader" class="">Leader</label>
                            <select name="leader" id="leader" class="form-control" required>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="simpan-dataleader" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>