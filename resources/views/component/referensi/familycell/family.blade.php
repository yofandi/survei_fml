@extends('apps')

@section('title')
<title>Family Cell</title>
@endsection

@section('content')
<div class="app-page-title">
	<div class="page-title-wrapper">
		<div class="page-title-heading">
			<div class="page-title-icon">
				<i class="pe-7s-notebook icon-gradient bg-mixed-hopes">
				</i>
			</div>
			<div>
				Family Cell
				<div class="page-title-subheading">Basic example of a Bootstrap 4 table with sort, search and filter functionality.
				</div>
			</div>
			<div class="page-title-actions">
				<div class="d-inline-block dropdown">
					<button type="button" onclick="Refresh()" class="btn-shadow btn btn-info">Refresh</button>
					{{-- <button type="button" class="btn btn-shadow btn-danger" data-toggle="modal" data-target=".bd-example-modal-lg">Tambah</button> --}}
					<button type="button" class="btn btn-shadow btn-danger add-data" data-title="Tambah Family Cell" data-toggle="modal" data-target=".bd-example-modal-lg">Tambah</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12" id="loader-table">
					
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="table-responsive">
				<table id="table-family" class="table table-hover table-striped table-bordered">
					<thead>
						<th>No.</th>
						<th>Nama</th>
						<th>Ketua</th>
						<th>Telefon</th>
						<th>Wilayah</th>
						<th>Alamat</th>
						<th>Aksi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
@include('component.referensi.familycell.modal')
{{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jqc-1.12.4/dt-1.10.20/datatables.min.js"></script>
@include('component.referensi.familycell.familyjs')
@endsection