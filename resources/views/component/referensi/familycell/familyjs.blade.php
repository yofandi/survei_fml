<script>
	let token = $("meta[name='csrf-token']").attr("content");
	// table
	var table = $('#table-family').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/familycell/get',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'nama_cell', name: 'nama_cell' },
		{ data: 'name', name: 'name' },
		{ data: 'telephon', name: 'telephon' },
		{ data: 'nama_wil', name: 'nama_wil' },
		{ data: 'alamat_cell', name: 'alamat_cell' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$button = '<button type="button" class="btn btn-xs btn-rounded btn-primary leader-sub"';
				$button += 'data-id="'+ o.id +'"';
				$button += 'data-nama_cell="'+ o.nama_cell +'"';
				$button += 'data-ketua="'+ o.ketua +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-xs btn-rounded btn-warning update-sub"';
				$button += 'data-id="'+ o.id +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += '><i class="fa fa-trash"></i></button>';
				return $button; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-family tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-family tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	// tambahan

	function Refresh() {
		$('#table-family').DataTable().ajax.reload();
	}

	function select_wilayah(like = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/wilayah/get',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			$html = '<option value="default">--- Pilih ----</option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'"';
				if (like == data[i].id) {
					$html += ' selected';
				}
				$html += '>'+ data[i].nama_wil +'</option>';
			}
			$('#wilayah').html($html);
		});
	}

	// alert setelah input
	function loader() {
		$gert = '<div class="loader-wrapper d-flex justify-content-center align-items-center">';
		$gert += '<div class="loader" align="center">';
		$gert += '<div class="ball-spin-fade-loader">';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '</div>';
		$gert += '</div>';
		$gert += '</div>';

		return $gert;
	}

	function alert(jenis, point, text) {
		$art = '<div class="alert alert-'+ jenis +' alert-dismissible fade show" role="alert">';
		$art += '<strong>'+ point +'</strong> ' + text;
		$art += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
		$art += '<span aria-hidden="true">&times;</span>';
		$art += '</button>';
		$art += '</div>';

		return $art;
	}

	// list user 

	function userlist(basedon,id = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/users/getleader',
			type: 'POST',
			dataType: 'json',
			data:  { fc: basedon, id:id }
		})
		.done(function(data) {
			$html = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'" ';
				if (data[i].id == id) {
					$html += 'selected';
				}
				$html += '>'+ data[i].name +'</option>';
			}
			$('#leader').html($html);
		});
	}

	$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
		$(".alert-dismissible").slideUp(500);
	});

	// crud table

	$(document).on('click', '.add-data', function(event) {
		let frt = $(this).data('title');
		select_wilayah();
		$('#form-data').attr('action', '/api/familycell/add');
		$('#form-data')[0].reset();
		$('#exampleModalLongTitle').text(frt);
	});

	$(document).on('click', '.update-sub', function(event) {
		let kare = $(this).data('id');

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/familycell/getRow',
			type: 'POST',
			dataType: 'json',
			data:  { id: kare }
		})
		.done(function(data) {
			$('#nama').val(data.nama_cell);
			$('#telefon').val(data.telephon);
			$('#alamat').val(data.alamat_cell);
			select_wilayah(data.tb_wilayah_id);
			
			$('#form-data').attr('action', '/api/familycell/upp/'+ data.id);
			$('#exampleModalLongTitle').text('Update family Cell - '+ data.nama_cell);
			$('.bd-example-modal-lg').modal('toggle');
		});
		
	});

	$(document).on('click', '#simpan-data', function(event) {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $('#form-data').attr('action'),
			type: 'POST',
			dataType: 'json',
			data: $('#form-data').serialize(),
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-hidden').html(erp);

				$('#table-family').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-hidden').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-hidden').html(nert);

			$('#form-data')[0].reset();
			$('#table-family').DataTable().ajax.reload();
		});
	});

	$(document).on('click', '#delete-row', function(event) {
		let crt = $(this).data('id');
		let jey = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jey) {
			$.ajax({
				headers: {
					'X-CSRF-Token': token 
				},
				url: '/api/familycell/delete',
				type: 'POST',
				dataType: 'json',
				data: {
					id: crt
				},
				error: function(xhr, data) { 
					alert(xhr.statusText + xhr.responseText);
					$jenis = 'danger';
					$point = 'Gagal';
					let erp = alert($jenis, $point, xhr.statusText);
					$('#loader-table').html(erp);
					$('#table-family').DataTable().ajax.reload();
				},
			})
			.done(function(data) {
				$jenis = 'success';
				$point = 'Berhasil';
				let nert = alert($jenis, $point, data);
				$('#loader-table').html(nert);
				$('#table-family').DataTable().ajax.reload();
			});
		}
	});

	// leader	

	$(document).on('click', '.leader-sub', function(event) {
		let kare = $(this).data('id');
		$('#form-dataleader').attr('action', '/api/familycell/chooseleader/'+ kare + '/' + $(this).data('ketua'));

		userlist(kare, $(this).data('ketua'));

		$('#exampleModalLongTitle1').text('Leader family Cell - '+ $(this).data('nama_cell'));
		$('.bd-example1-modal-lg').modal('toggle');
	});

	$(document).on('click', '#simpan-dataleader', function(event) {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $('#form-dataleader').attr('action'),
			type: 'POST',
			dataType: 'json',
			data: $('#form-dataleader').serialize(),
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-hidden-leader').html(erp);

				$('#table-family').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-hidden-leader').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-hidden-leader').html(nert);

			$('#form-dataleader')[0].reset();
			$('#table-family').DataTable().ajax.reload();
		});
	});
</script>