<script>
	$('#grid_kategori').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: '/api/kategori/get',
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: '/api/kategori/add',
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: '/api/kategori/edit',
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: '/api/kategori/delete',
					data: item
				});
			},
		},

		fields: [
		{
			name: "id",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "nama_kategori",
			title: "Kategori",
			type: "text", 
			width: 50
		},
		{
			type: "control"
		}
		]
	});
	
	function Refresh() {
		$("#grid_kategori").jsGrid("loadData");
	}
</script>