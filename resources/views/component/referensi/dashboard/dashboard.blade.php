@extends('apps')

@section('title')
<title>Dashboard</title>
@endsection

@section('content')
<div class="app-page-title">
	<div class="page-title-wrapper">
		<div class="page-title-heading">
			<div class="page-title-icon">
				<i class="pe-7s-car icon-gradient bg-mean-fruit">
				</i>
			</div>
			<div>
				Dashboard
				<div class="page-title-subheading">This is an example dashboard created using build-in elements and components.
				</div>
			</div>
		</div>
		<div class="page-title-actions">
			<button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark" data-original-title="Example Tooltip">
				<i class="fa fa-star"></i>
			</button>
			<div class="d-inline-block dropdown">
				<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
					<span class="btn-icon-wrapper pr-2 opacity-7">
						<i class="fa fa-business-time fa-w-20"></i>
					</span>
					Buttons
				</button>
			</div>
		</div>    
	</div>
</div>

<div class="main-card mb-3 card">
	<div class="card-header-tab card-header">
		{{-- <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
			<i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>
			Grafik
		</div> --}}
		<div class="btn-actions-pane-right text-capitalize">
			<select class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm" id="tahun">
				{{-- <option value="">All</option> --}}
				@for ($i = date('Y'); $i > date('Y') - 35; $i--)
					<option value="{{ $i }}">{{ $i }}</option>
				@endfor
			</select>
			<select class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm" id="bulan">
				<option value="">All</option>
				@foreach ($hery as $herys => $herie)
					<option value="{{ $herys }}">{{ $herie }}</option>
				@endforeach
			</select>
			<button class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm send-datagrap">Search</button>
			<button class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm" onclick="refresh()">Reset</button>
		</div>
	</div>
	<div class="card-body">
		<div class="col-md-12">
			{{-- <h5 class="card-title">Line Chart</h5> --}}
			<div style="height: 400px">
				<canvas id="myChart"></canvas>
			</div>
		</div>
	</div>
	<div class="card-header-tab card-header">
		<div class="btn-actions-pane-right text-capitalize">
		</div>
	</div>
</div>
@endsection

@section('js')
@include('component.referensi.dashboard.dashboardjs')
@endsection