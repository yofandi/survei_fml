<script>
    let token = $('meta[name="csrf-token"]').attr('content');
    var ctx = document.getElementById('myChart');

    function chart(labels,data,years) {
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: data
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Grafik Jumlah Cell'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: years
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Quantity'
                        }
                    }]
                }
            }
        });
    }

    $(document).on('click', '.send-datagrap', function(event) {
        let tahun = $('#tahun').val();
        let bulan = $('#bulan').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '/api/dashboard/chart',
            type: 'POST',
            dataType: 'json',
            data: {
                tahun: tahun,
                bulan: bulan,
            },
        })
        .done(function(data) {
            $geh = datas('Jml Dewasa', data.dewasa, 'Jml Anak', data.anak, 'Jml Jemaat Baru', data.jemaat_baru);

            chart( data.bulan, $geh, data.tahun);
        });
        
    });

    function datas(label, data, label1, data1, label2, data2) {
        $return = [{
            label: label,
            fill: false,
            data: data,
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
        },
        {
            label: label1,
            fill: false,
            data: data1,
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
        },
        {
            label: label2,
            fill: false,
            data: data2,
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
        }];

        return $return;
    }

    function begining() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '/api/dashboard/chart',
            type: 'POST',
            dataType: 'json',
            data: {
                tahun: {{ date('Y') }},
                bulan: '',
            },
        })
        .done(function(data) {
            // console.log(data);
            $geh = datas('Jml Dewasa', data.dewasa, 'Jml Anak', data.anak, 'Jml Jemaat Baru', data.jemaat_baru);

            chart( data.bulan, $geh, data.tahun);
        });
    }

    function refresh() {
        begining();
    }

    $(function() {
        begining();
    });
</script>