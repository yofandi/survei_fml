<script>
	$('#divpass').hide();
	$('#nav_hidden').hide();
	$('#gmbr-hidden').hide();

	let token = $("meta[name='csrf-token']").attr("content");

	function tampilkanPreview(gambar,idpreview){
		var gb = gambar.files;
		for (var i = 0; i < gb.length; i++){
			var gbPreview = gb[i];
			var imageType = /image.*/;
			var preview=document.getElementById(idpreview);            
			var reader = new FileReader();

			if (gbPreview.type.match(imageType)) {
				preview.file = gbPreview;
				reader.onload = (function(element) { 
					return function(e) { 
						element.src = e.target.result; 
					}; 
				})(preview);
				reader.readAsDataURL(gbPreview);
			}else{
				alert("file yang anda upload tidak sesuai. Khusus mengunakan image.");
			}

		}    
	}

	const kategori = {!! json_encode($kategori) !!}
	kategori.unshift({id: '',nama_kategori: ''});

	var table = $('#table-users').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/users/get',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'name', name: 'name' },
		{ data: 'telephon_agt', name: 'telephon_agt' },
		{ data: 'nama_kategori', name: 'nama_kategori' },
		{ data: 'nama_cell', name: 'nama_cell' },
		{ data: 'alamat_agt', name: 'alamat_agt' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$button = '<button type="button" class="btn btn-xs btn-rounded btn-primary password-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += 'data-name="'+ o.name +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-xs btn-rounded btn-warning update-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += 'data-id_wilayah="'+ o.tb_wilayah_id +'"';
				$button += '><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-row"';
				$button += 'data-id="'+ o.id +'"';
				$button += '><i class="fa fa-trash"></i></button>';
				return $button; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-users tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-users tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function loader() {
		$gert = '<div class="loader-wrapper d-flex justify-content-center align-items-center">';
		$gert += '<div class="loader" align="center">';
		$gert += '<div class="ball-spin-fade-loader">';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '<div></div>';
		$gert += '</div>';
		$gert += '</div>';
		$gert += '</div>';

		return $gert;
	}

	function alert(jenis, point, text) {
		$art = '<div class="alert alert-'+ jenis +' alert-dismissible fade show" role="alert">';
		$art += '<strong>'+ point +'</strong> ' + text;
		$art += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
		$art += '<span aria-hidden="true">&times;</span>';
		$art += '</button>';
		$art += '</div>';

		return $art;
	}

	$('#tanggal_lahir').datepicker({
		format: 'yyyy-mm-dd',
	});

	$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
		$(".alert-dismissible").slideUp(500);
	});

	function Refresh() {
		$('#table-users').DataTable().ajax.reload();
	}

	function kategorilist(like = '') {
		$html = '';
		for (var i = 0; i < kategori.length; i++) {
			$html += '<option value="'+ kategori[i].id +'"';
			if (like == kategori[i].id) {
				$html += ' selected';
			}
			$html += '>'+ kategori[i].nama_kategori +'</option>';
		}
		$('#kategori').html($html);
	}

	function select_wilayah(like = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/wilayah/get',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			$html = '<option value="default">--- Pilih ----</option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'"';
				if (like == data[i].id) {
					$html += ' selected';
				}
				$html += '>'+ data[i].nama_wil +'</option>';
			}
			$('#wilayah').html($html);
		});
	}

	function select_familycell(basedon, like = '') {
		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/familycell/getlist',
			type: 'POST',
			dataType: 'json',
			data: { wilid: basedon }
		})
		.done(function(data) {
			$html = '<option value="default">--- Pilih ----</option>';
			for (var i = 0; i < data.length; i++) {
				$html += '<option value="'+ data[i].id +'"';
				if (like == data[i].id) {
					$html += ' selected';
				}
				$html += '>'+ data[i].nama_cell +'</option>';
			}
			$('#family_cell').html($html);
		});	
	}

	$(document).on('change', '#wilayah', function(event) {
		let pure = $(this).val();
		select_familycell(pure);
	});

	$(document).on('click', '.add-data', function(event) {
		let frt = $(this).data('title');

		$('#divpass').fadeIn();
		$('#nav_hidden').hide();
		$('#gmbr-hidden').fadeIn();

		select_wilayah();

		$('#form-data').attr('action', '/api/users/add');
		$('#form-data')[0].reset();
		$('#exampleModalLongTitle').text(frt);
	});

	$(document).on('click', '.update-row', function(event) {
		let kare = $(this);
		$('#form-image')[0].reset();

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: '/api/users/getRow',
			type: 'POST',
			dataType: 'json',
			data:  { id: kare.data('id') }
		})
		.done(function(data) {
			$('#divpass').hide();
			$('#nav_hidden').fadeIn();
			$('#gmbr-hidden').hide();

			$('#nama').val(data.name);
			$('#tempat_lahir').val(data.tempat_lahir);
			$('#tanggal_lahir').val(data.tanggal_lahir);
			$('#email').val(data.email);
			// $('#password').val(data.password);
			$('#telefon').val(data.telephon_agt);
			$('#alamat').val(data.alamat_agt);

			$('#preview').attr('src', '/images/'+data.foto_agt);

			select_wilayah(kare.data('id_wilayah'));
			select_familycell(kare.data('id_wilayah'),data.tb_familycell_id_fc);

			$(document).on('change', '#wilayah', function(event) {
				let pure = $(this).val();
				select_familycell(pure);
			});

			$('#form-data').attr('action', '/api/users/upp/'+ data.id);
			$('#form-image').attr('action', '/api/users/updatefoto/'+ data.id);
			$('#exampleModalLongTitle').text('Update Anggota - '+ data.name);
			$('.bd-example-modal-lg').modal('toggle');
		});
		
	});

	$(document).on('click', '.password-row', function(event) {
		let data = $(this).data('name');
		$('#divpass').hide();

		select_wilayah();

		$('#form-datapas').attr('action', '/api/users/newPassword/' + $(this).data('id') );
		$('#form-datapas')[0].reset();
		$('#exampleModalLongTitle1').text('Ganti Password Anggota - '+ data);
		$('.bd-example1-modal-lg').modal('toggle');
	});

	$(document).on('click', '#simpan-data', function(event) {
		var form = $("#form-data");

		var formData = new FormData(form[0]);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $(form).prop("action"),
			type: 'POST',
			dataType: 'json',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			// enctype: 'multipart/form-data',
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-hidden').html(erp);

				$('#table-users').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-hidden').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-hidden').html(nert);

			$('#form-data')[0].reset();
			$('#table-users').DataTable().ajax.reload();
		});
	});

	$(document).on('click', '#delete-row', function(event) {
		let crt = $(this).data('id');
		let jey = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jey) {
			$.ajax({
				headers: {
					'X-CSRF-Token': token 
				},
				url: '/api/users/delete',
				type: 'POST',
				dataType: 'json',
				data: {
					id: crt
				},
				error: function(xhr, data) { 
					alert(xhr.statusText + xhr.responseText);
					$jenis = 'danger';
					$point = 'Gagal';
					let erp = alert($jenis, $point, xhr.statusText);
					$('#loader-table').html(erp);
					$('#table-family').DataTable().ajax.reload();
				},
			})
			.done(function(data) {
				$jenis = 'success';
				$point = 'Berhasil';
				let nert = alert($jenis, $point, data);
				$('#loader-table').html(nert);
				$('#table-users').DataTable().ajax.reload();
			});
		}
	});

	$(document).on('click', '#simpan-datapas', function(event) {
		let pass1 = $('#password_baru').val();
		let pass2 = $('#konfirmasi_password').val();
		if (pass1 == pass2) {
			$.ajax({
				headers: {
					'X-CSRF-Token': token 
				},
				url: $('#form-datapas').attr('action'),
				type: 'POST',
				dataType: 'json',
				data: $('#form-datapas').serialize(),
				error: function(xhr, data) { 
					alert(xhr.statusText + xhr.responseText);
					$jenis = 'danger';
					$point = 'Gagal';
					let erp = alert($jenis, $point, xhr.statusText);
					$('#loader-hidden-pass').html(erp);

					$('#table-users').DataTable().ajax.reload();
				},
				beforeSend: function() {
					let prop = loader();
					$('#loader-hidden-pass').html(prop);
				}
			})
			.done(function(data) {
				$jenis = 'success';
				$point = 'Berhasil';
				$text = data;
				let nert = alert($jenis, $point, $text);
				$('#loader-hidden-pass').html(nert);

				$('#form-datapas')[0].reset();
				$('#table-users').DataTable().ajax.reload();
			});
		} else {
			$jenis = 'danger';
			$point = 'Gagal';
			let erp = alert($jenis, $point, 'Password tidak sama!!!');
			$('#loader-hidden-pass').html(erp);
		}
	});

	$(document).on('click', '#simpan-image', function(event) {
		var form = $("#form-image");

		var formData = new FormData(form[0]);

		$.ajax({
			headers: {
				'X-CSRF-Token': token 
			},
			url: $(form).prop("action"),
			type: 'POST',
			dataType: 'json',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			// enctype: 'multipart/form-data',
			error: function(xhr, data) { 
				alert(xhr.statusText + xhr.responseText);
				$jenis = 'danger';
				$point = 'Gagal';
				let erp = alert($jenis, $point, xhr.statusText);
				$('#loader-image').html(erp);

				$('#table-users').DataTable().ajax.reload();
			},
			beforeSend: function() {
				let prop = loader();
				$('#loader-image').html(prop);
			}
		})
		.done(function(data) {
			$jenis = 'success';
			$point = 'Berhasil';
			$text = data;
			let nert = alert($jenis, $point, $text);
			$('#loader-image').html(nert);

			$('#form-image')[0].reset();
			$('#table-users').DataTable().ajax.reload();
		});
	});
</script>