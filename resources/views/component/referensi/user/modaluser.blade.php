<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="nav_hidden">
                    <li class="nav-item">
                        <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                            <span>Form Data</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                            <span>Image</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">

                        <div class="row">
                            <div class="col-md-12" id="loader-hidden">
                            </div>
                        </div>
                        <form id="form-data" method="POST" enctype="multipart/form-data">
                            <div class="modal-body">
                                {{-- <h5 class="card-title">Grid Rows</h5> --}}
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="position-relative form-group">
                                            <label for="nama" class="">Nama</label>
                                            <input name="nama" id="nama" placeholder="Nama" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="tempat_lahir" class="">Tempat Lahir</label>
                                            <input name="tempat_lahir" id="tempat_lahir" placeholder="Tempat" type="text" class="form-control"  required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="tanggal_lahir" class="">Tanggal Lahir</label>
                                            <input name="tanggal_lahir" id="tanggal_lahir" placeholder="00-00-0000" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <label for="email" class="">Email</label>
                                                <input name="email" id="email" placeholder="...@gmail.com" type="email" class="form-control"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="divpass">
                                            <div class="position-relative form-group">
                                                <label for="password" class="">Password</label>
                                                <input name="password" id="password" placeholder="................." type="password" class="form-control"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="position-relative form-group">
                                                <label for="wilayah" class="">Wilayah</label>
                                                <select name="wilayah" id="wilayah" class="form-control" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="position-relative form-group">
                                                <label for="family_cell" class="">Family Cell</label>
                                                <select name="family_cell" id="family_cell" class="form-control" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="position-relative form-group">
                                                <label for="telefon" class="">Telefon</label>
                                                <input name="telefon" id="telefon" placeholder="08######" type="text" class="form-control"  required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative form-group">
                                    <label for="alamat" class="">Alamat</label>
                                    <textarea name="alamat" id="alamat" placeholder="............." class="form-control"></textarea>
                                </div>
                                <div class="position-relative form-group" id="gmbr-hidden">
                                    <label for="gambar" class="">Gambar</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="simpan-data" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane tabs-animation fade show " id="tab-content-1" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" id="loader-image">
                            </div>
                        </div>
                        <form id="form-image" method="POST" enctype="multipart/form-data">
                            <div class="modal-body">
                                <h5 class="card-title">Foto</h5>
                                <div class="form-row">
                                    <div class="col-md-12" align="center">
                                        <img id="preview" height="300" width="450" class="img-circle" alt="User Image"/>
                                    </div>
                                </div>
                                <div class="position-relative form-group">
                                    <input accept="image/*" onchange="tampilkanPreview(this,'preview')" type="file" name="image" id="image_update" class="form-control-file">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="simpan-image" class="btn btn-success">Save changes</button>
                            </div>
                        </form>
                    </div>      
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle1"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" id="loader-hidden-pass">
                    </div>
                </div>
                <form id="form-datapas" method="POST">
                    <div class="modal-body">
                        {{-- <h5 class="card-title">Grid Rows</h5> --}}
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="password" class="">Password Baru</label>
                                    <input name="password" id="password_baru" placeholder="" type="password" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="konfirmasi_password" class="">Konfirmasi Password</label>
                                    <input name="konfirmasi_password" id="konfirmasi_password" placeholder="" type="password" class="form-control"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="simpan-datapas" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>