@extends('apps')

@section('title')
<title>Users</title>
@endsection

@section('content')
<div class="app-page-title">
	<div class="page-title-wrapper">
		<div class="page-title-heading">
			<div class="page-title-icon">
				<i class="pe-7s-notebook icon-gradient bg-mixed-hopes">
				</i>
			</div>
			<div>
				Users
				<div class="page-title-subheading">Basic example of a Bootstrap 4 table with sort, search and filter functionality.
				</div>
			</div>
			<div class="page-title-actions">
				<div class="d-inline-block dropdown">
					<button type="button" onclick="Refresh()" class="btn-shadow btn btn-info">Refresh</button>
					{{-- <button type="button" class="btn btn-shadow btn-danger" data-toggle="modal" data-target=".bd-example-modal-lg">Tambah</button> --}}
					<button type="button" class="btn btn-shadow btn-danger add-data" data-title="Tambah Anggota" data-toggle="modal" data-target=".bd-example-modal-lg">Tambah</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-card mb-3 card">
	<div class="card-body">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12" id="loader-table">
					
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="table-responsive">
				<table id="table-users" class="table table-hover table-striped table-bordered">
					<thead>
						<th>No.</th>
						<th>Nama</th>
						<th>Telefon</th>
						<th>Kategori</th>
						<th>Family Cell</th>
						<th>Alamat</th>
						<th>Aksi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
@include('component.referensi.user.modaluser')
@include('component.referensi.user.userjs')
@endsection