<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'name', 
        // 'email', 
        // 'password',
        'name',
        'email',
        'password',
        'telephon_agt',
        'tempat_lahir',
        'tanggal_lahir',
        'foto_agt',
        'alamat_agt',
        'tb_familycell_id_fc',
        'tb_kategori_id',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\model\Tb_kategori','tb_kategori_id');
    }

    private function getUserRole()
    {
       return $this->role()->getResults();
    }
 
    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->cekUserRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->cekUserRole($roles);
        }
        return false;
    }

    private function cekUserRole($role)
    {
        return (strtolower($role)==strtolower($this->have_role->id)) ? true : false;
    }
}
