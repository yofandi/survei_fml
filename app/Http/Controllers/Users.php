<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\model\Tb_kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class Users extends Controller
{
	public function __construct()
    {
        // $this->middleware('auth');
    }
	
	public function index()
	{
		$data['kategori'] = Tb_kategori::whereNotIn('id', [1])->get();
		return view('component.referensi.user.userview', $data);
	}

	public function get()
	{
		$app = DB::table('users')
		->select('users.*','tb_kategori.nama_kategori','tb_familycell.nama_cell','tb_familycell.tb_wilayah_id')
		->leftJoin('tb_kategori','users.tb_kategori_id','=','tb_kategori.id')
		->leftJoin('tb_familycell','users.tb_familycell_id_fc','=','tb_familycell.id')
		->leftJoin('tb_wilayah', 'tb_familycell.tb_wilayah_id', '=', 'tb_wilayah.id')
		->whereNotIn('tb_kategori.id', [1])
		->get();

		return DataTables::of($app)
		->addIndexColumn()
		->make(true);
	}

	public function getleader(Request $request)
	{
		$app = User::where('tb_familycell_id_fc', $request->fc)->get();
		return $app->toJson();	
	}

	public function getRow(Request $request)
	{
		$app = User::where('id', $request->id)->first();
		return $app->toJson();
	}

    public function doupload($request)
    {
        request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240']);
        if ($files = $request->file('image')) {
            $profileImage = 'familycell' . date('YmdHis') . "." . $files->getClientOriginalExtension();

            Storage::disk('upload')->put($profileImage,  File::get($files));

            $img = "$profileImage";
        }

        return $img;
    }

	public function add(Request $request)
	{
		$image = $this->doupload($request);

		$data = [
			'name' => $request->nama,
			'telephon_agt' => $request->telefon,
			'tempat_lahir' => $request->tempat_lahir,
			'tanggal_lahir' => $request->tanggal_lahir,
			'alamat_agt' => $request->alamat,
			'foto_agt' => $image,
			'email' => $request->email,
			'password' => Hash::make($request->password),
			'created_at' => date('Y-m-d'),
			'tb_familycell_id_fc' => $request->family_cell,
			'tb_kategori_id' => 3,
		];

		$create = User::create($data);

		if ($create) {
			$result = 'Berhasil tambah data anggota';
		} else {
			$result = 'Gagal tambah data anggota';
		}
		
		return json_encode($result);
	}

	public function update(Request $request,$id)
	{
		$data = [
			'name' => $request->nama,
			'telephon_agt' => $request->telefon,
			'tempat_lahir' => $request->tempat_lahir,
			'tanggal_lahir' => $request->tanggal_lahir,
			'alamat_agt' => $request->alamat,
			'email' => $request->email,
			// 'password' => Hash::make($request->password),
			'created_at' => date('Y-m-d'),
			'tb_familycell_id_fc' => $request->family_cell,
			'tb_kategori_id' => 3,
		];

		$update = User::where('id', $id)->update($data);

		if ($update) {
			$result = 'Berhasil update data anggota '.$id;
		} else {
			$result = 'Gagal update data anggota '.$id;
		}
		
		return json_encode($result);
	}

	public function edit_foto(Request $request,$id)
    {
        $cek = User::find($id);
        if ($cek->foto_agt != '' || !empty($cek->foto_agt)) {
            $link = public_path().'/images/'.$cek->foto_agt;
            File::delete($link);
        }

        $image = $this->doupload($request);

        $query = User::where('id',$id)
        ->update(['foto_agt' => $image]);
        if ($query > 0) {
            $result = 'Berhasil update foto anggota '.$id;
        } else {
            $result = 'Gagal update foto anggota '.$id;
        }
		return json_encode($result);
    }

	public function delete(Request $request)
	{
        $cek = User::find($id);
        if ($cek->foto_agt != '' || !empty($cek->foto_agt)) {
            $link = public_path().'/images/'.$cek->foto_agt;
            File::delete($link);
        }

		$app = User::find($request->id)->delete();

		if ($app) {
			$result = 'Berhasil hapus data anggota '.$request->id.'';
		} else {
			$result = 'Gagal hapus data anggota '.$request->id.'';
		}
		
		return json_encode($result);		
	}

	public function newPassword(Request $request,$id)
	{
		$data = [
			'password' => Hash::make($request->password),
		];

		$update = User::where('id', $id)->update($data);

		if ($update) {
			$result = 'Berhasil update password anggota '.$id;
		} else {
			$result = 'Gagal update password anggota '.$id;
		}
		
		return json_encode($id);	
	}
}
