<?php

namespace App\Http\Controllers;

// use Datatables;
use Yajra\Datatables\Datatables;
use App\User;
use App\model\Tb_familycell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Familycell extends Controller
{
	public function __construct()
    {
        // $this->middleware('auth');
    }
    
	public function index()
	{
		return view('component.referensi.familycell.family');
	}

	public function get()
	{
		$app = DB::table('tb_familycell')
		->select('tb_familycell.*','tb_wilayah.nama_wil','users.name')
		->leftJoin('users','tb_familycell.ketua','=','users.id')
		->leftJoin('tb_wilayah', 'tb_familycell.tb_wilayah_id', '=', 'tb_wilayah.id')
		->get();
		return DataTables::of($app)
		->addIndexColumn()
		->make(true);
	}

	public function getlist(Request $request)
	{
		$app = Tb_familycell::where('tb_wilayah_id', $request->wilid)->get();
		return $app->toJson();
	}

	public function getRow(Request $request)
	{
		$app = Tb_familycell::where('id', $request->id)->first();
		return $app->toJson();
	}

	public function add(Request $request)
	{
		$data = [
			'nama_cell' => $request->nama,
			'alamat_cell' => $request->alamat,
			'telephon' => $request->telefon,
			'tb_wilayah_id' => $request->wilayah,
		];

		$create = Tb_familycell::create($data);

		if ($create) {
			$result = 'Berhasil tambah data Family Cell';		
		} else {
			$result = 'Gagal tambah data Family Cell';
		}
		
		return json_encode($result);
	}

	public function update(Request $request,$id)
	{
		$data = [
			'nama_cell' => $request->nama,
			'alamat_cell' => $request->alamat,
			'telephon' => $request->telefon,
			'tb_wilayah_id' => $request->wilayah,
		];

		$create = Tb_familycell::where('id',$id)->update($data);

		if ($create) {
			$result = 'Berhasil update data Family Cell '.$id.'';
		} else {
			$result = 'Gagal update data Family Cell '.$id.'';
		}
		
		return json_encode($result);
	}

	public function delete(Request $request)
	{
		$app = Tb_familycell::find($request->id)->delete();

		if ($app) {
			$result = 'Berhasil hapus data Family Cell '.$request->id.'';
		} else {
			$result = 'Gagal hapus data Family Cell '.$request->id.'';
		}
		
		return json_encode($result);			
	}

	public function chooseleader(Request $request,$id,$ketua)
	{
		$data = [
			'ketua' => $request->leader
		];

		$hh = User::where('id', $ketua);
		if ($hh->count() > 0) {	

			$data1 = [
				'tb_kategori_id' => 3,
			];

			$update0 = User::where('id', $ketua)->update($data1);
			$update1 = User::where('id', $request->leader)->update(['tb_kategori_id' => 2]);
		}

		$create = Tb_familycell::where('id',$id)->update($data);
		if ($create) {
			$result = 'Berhasil memilih Leader Family Cell '.$id.'';
		} else {
			$result = 'Gagal memilih Leader Family Cell '.$id.'';
		}
		return json_encode($result);	
	}
}
