<?php

namespace App\Http\Controllers;

use App\User;
use App\model\Tb_kegiatan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        $data['api_token'] = Auth::user()->api_token;
        $data['hery'] = [];
        for ($i=1; $i <= 12; $i++) { 

            if ($i < 10) {
                $j = '0'.$i;

                $g = date('Y').'-0'.$i;
            } else {
                $j = $i;

                $g = date('Y').'-'.$i;
            }

            $tr = date_create($g);
            $data['hery'][$j] = date_format($tr, 'F');
        }
        return view('component.referensi.dashboard.dashboard', $data);
    }

    public function getGrafil(Request $request)
    {
        $vat = $request->tahun.'-'.$request->bulan;
        $daef = date_create($vat);
        $val = ( $request->bulan == '' ? 12 : date_format($daef,'t') );
        $bulan0 = [];
        $bulan1 = [];
        $bulan2 = [];
        $bulan3 = [];
        $ghe = 0;
        $obt = [];
        for ($i=1; $i <= $val; $i++) { 

            if ($i < 10) {
                $j = '0'.$i;

                $g = $request->tahun.'-0'.$i;
            } else {
                $j = $i;

                $g = $request->tahun.'-'.$i;
            }

            $nk = (string) $i;
            $k = (string) $j;
            $tr = date_create($g);

            $query = DB::table('tb_kegiatan')
            ->select(DB::raw('SUM(jml_dewasa) AS jml_dewasa,
                SUM(jml_anak) AS jml_anak,
                SUM(jml_jemaatbaru) AS jml_jemaatbaru'));
            if ($request->bulan != '') {
                $query->where([
                    [DB::raw('date_format(tanggal_keg, "%d")'),$k],
                    [DB::raw('date_format(tanggal_keg, "%m")'),$request->bulan],
                ]);
            } else {
                $query->where([
                    [DB::raw('date_format(tanggal_keg, "%m")'),$k],
                ]);
            }
            $query->where([
                [DB::raw('date_format(tanggal_keg, "%Y")'),$request->tahun]
            ]);
            $getthis = $query->first();

            $bulan0[] = ($val == 12 ? date_format($tr, 'F') : $j);

            $bulan1[] = ($getthis->jml_dewasa == null ? 0 : (int) $getthis->jml_dewasa);
            $bulan2[] = ($getthis->jml_anak == null ? 0 : (int) $getthis->jml_anak);
            $bulan3[] = ($getthis->jml_jemaatbaru == null ? 0 : (int) $getthis->jml_jemaatbaru);
        }
        $obj['tahun'] = $request->tahun;
        $obj['bulan'] = $bulan0;
        $obj['dewasa'] = $bulan1;
        $obj['anak'] = $bulan2;
        $obj['jemaat_baru'] = $bulan3;
        return response()->json($obj);
    }
}
