<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\model\Tb_kegiatan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

class Kegiatan extends Controller
{
	public function __construct()
    {
        // $this->middleware('auth');
    }
    
	public function index()
	{
		return view('component.referensi.kegiatan.kegiatan');
	}

	public function get()
	{
		$app = DB::table('tb_kegiatan')
		->select('tb_kegiatan.*','tb_familycell.nama_cell','tb_familycell.tb_wilayah_id','tb_wilayah.nama_wil')
		->leftJoin('tb_familycell','tb_kegiatan.tb_familycell_id','=','tb_familycell.id')
		->leftJoin('tb_wilayah','tb_familycell.tb_wilayah_id','=','tb_wilayah.id')
		->get();

		return DataTables::of($app)
		->addIndexColumn()
		->make(true);
	}

	public function getRow(Request $request)
	{
		$app = Tb_kegiatan::where('id', $request->id)->first();
		return $app->toJson();
	}

    public function doupload($request)
    {
        request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240']);
        if ($files = $request->file('image')) {
            $profileImage = 'familycell' . date('YmdHis') . "." . $files->getClientOriginalExtension();

            Storage::disk('kegupload')->put($profileImage,  File::get($files));

            $img = "$profileImage";
        }

        return $img;
    }

	public function add(Request $request)
	{
		$image = $this->doupload($request);

		$data = [
			'tb_familycell_id' => $request->family_cell,
			'tanggal_keg' => $request->tanggal,
			'lokasi_keg' => $request->lokasi,
			'jml_dewasa' => $request->jml_dws,
			'jml_anak' => $request->jml_ank,
			'jml_jemaatbaru' => $request->jml_jmtbr,
			'total' => $request->total,
			'foto_kegiatan' => $image,
		];

		$create = Tb_kegiatan::create($data);

		if ($create) {
			$result = 'Berhasil tambah data kegiatan';
		} else {
			$result = 'Gagal tambah data kegiatan';
		}
		
		return json_encode($result);
	}

	public function update(Request $request, $id)
	{
		$data = [
			'tb_familycell_id' => $request->family_cell,
			'tanggal_keg' => $request->tanggal,
			'lokasi_keg' => $request->lokasi,
			'jml_dewasa' => $request->jml_dws,
			'jml_anak' => $request->jml_ank,
			'jml_jemaatbaru' => $request->jml_jmtbr,
			'total' => $request->total,
			// 'foto_kegiatan' => $image,
		];

		$create = Tb_kegiatan::where('id', $id)->update($data);

		if ($create) {
			$result = 'Berhasil tambah data kegiatan';
		} else {
			$result = 'Gagal tambah data kegiatan';
		}
		
		return json_encode($result);
	}

	public function edit_foto(Request $request, $id)
	{
        $cek = Tb_kegiatan::find($id);
        if ($cek->foto_kegiatan != '' || !empty($cek->foto_kegiatan)) {
            $link = public_path().'/images/kegiatan/'.$cek->foto_kegiatan;
            File::delete($link);
        }

        $image = $this->doupload($request);

		$data = [
			'foto_kegiatan' => $image,
		];

		$create = Tb_kegiatan::where('id', $id)->update($data);

		if ($create) {
			$result = 'Berhasil Update foto kegiatan '.$id;
		} else {
			$result = 'Gagal Update foto kegiatan '.$id;
		}
		
		return json_encode($result);
	}

	public function delete(Request $request)
	{
        $cek = Tb_kegiatan::find($request->id);
        if ($cek->foto_kegiatan != '' || !empty($cek->foto_kegiatan)) {
            $link = public_path().'/images/kegiatan/'.$cek->foto_kegiatan;
            File::delete($link);
        }

		$app = Tb_kegiatan::find($request->id)->delete();

		if ($app) {
			$result = 'Berhasil hapus data kegiatan '.$request->id.'';
		} else {
			$result = 'Gagal hapus data kegiatan '.$request->id.'';
		}
		
		return json_encode($result);		
	}
}
