<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Tb_kategori;

class Kategori extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    public function index()
    {
    	return view('component.referensi.kategori.kateg');
    }

    public function get(Request $request)
    {
        $query = Tb_kategori::where('nama_kategori', 'LIKE', "%$request->nama_kategori%")
        ->get();
        return $query->toArray();
    }

    public function add(Request $request)
    {
        $data = [
            'nama_kategori' => $request->nama_kategori
        ];
        $query = Tb_kategori::create($data);
    }

    public function put(Request $request)
    {
        $data = [
            'nama_kategori' => $request->nama_kategori
        ];
        $query = Tb_kategori::where('id',$request->id)->update($data);
    }

    public function delete(Request $request)
    {
        $query = Tb_kategori::where('id',$request->id)
        ->delete();
    }
}
