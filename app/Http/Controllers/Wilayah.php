<?php

namespace App\Http\Controllers;

use App\model\Tb_wilayah;
use Illuminate\Http\Request;

class Wilayah extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    
    public function index()
    {
    	return view('component.referensi.wilayah.wila');
    }

    public function get(Request $request)
    {
        $query = Tb_wilayah::where('nama_wil', 'LIKE', "%$request->nama_wil%")
        ->get();
        return $query->toArray();
    }

    public function add(Request $request)
    {
        $data = [
            'nama_wil' => $request->nama_wil
        ];
        $query = Tb_wilayah::create($data);
    }

    public function put(Request $request)
    {
        $data = [
            'nama_wil' => $request->nama_wil
        ];
        $query = Tb_wilayah::where('id',$request->id)->update($data);
    }

    public function delete(Request $request)
    {
        $query = Tb_wilayah::where('id',$request->id)
        ->delete();
    }
}
