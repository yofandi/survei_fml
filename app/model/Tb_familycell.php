<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tb_familycell extends Model
{
    protected $table = 'tb_familycell';

    protected $fillable = [
        'nama_cell','alamat_cell','ketua','telephon','foto_familycell','tb_wilayah_id'
    ];
}
