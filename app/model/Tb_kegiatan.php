<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tb_kegiatan extends Model
{
	protected $table = 'tb_kegiatan';

	protected $fillable = [
		'tb_familycell_id',
		'tanggal_keg',
		'lokasi_keg',
		'jml_dewasa',
		'jml_anak',
		'jml_jemaatbaru',
		'total',
		'foto_kegiatan',
	];
}
