<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tb_kategori extends Model
{
    protected $table = 'tb_kategori';

    protected $fillable = [
        'nama_kategori'
    ];
}
