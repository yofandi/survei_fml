<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tb_wilayah extends Model
{
    protected $table = 'tb_wilayah';

    protected $fillable = [
        'nama_wil'
    ];
}
