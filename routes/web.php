<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['web','auth','roles']], function () {

	Route::get('/', function () {
		return view('apps');
	});

	Route::get('/kategori', 'Kategori@index');
	Route::get('/wilayah', 'Wilayah@index');
	Route::get('/familycell', 'Familycell@index');
	Route::get('/users', 'Users@index');
	Route::get('/kegiatan', 'Kegiatan@index');
	Route::get('/dashboard', 'HomeController@dashboard');
	Route::get('/test/bulan', 'HomeController@getGrafil');

	Route::get('/home', 'HomeController@index')->name('home');
});

