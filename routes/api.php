<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
// Route::group(['middleware' => ['web','auth:api','roles']], function() {
	Route::post('/dashboard/chart', 'HomeController@getGrafil');

	Route::get('/kategori/get', 'Kategori@get');
	Route::post('/kategori/add', 'Kategori@add');
	Route::put('/kategori/edit', 'Kategori@put');
	Route::delete('/kategori/delete', 'Kategori@delete');


	Route::get('/wilayah/get', 'Wilayah@get');
	Route::post('/wilayah/add', 'Wilayah@add');
	Route::put('/wilayah/edit', 'Wilayah@put');
	Route::delete('/wilayah/delete', 'Wilayah@delete');

	Route::get('/familycell/get', 'Familycell@get');
	Route::post('/familycell/getlist', 'Familycell@getlist');
	Route::post('/familycell/getRow', 'Familycell@getRow');
	Route::post('/familycell/add', 'Familycell@add');
	Route::post('/familycell/upp/{id}', 'Familycell@update');
	Route::post('/familycell/delete', 'Familycell@delete');
	Route::post('/familycell/chooseleader/{id}/{ketua}', 'Familycell@chooseleader');

	Route::get('/users/get', 'Users@get');
	Route::post('/users/getRow', 'Users@getRow');
	Route::post('/users/add', 'Users@add');
	Route::post('/users/upp/{id}', 'Users@update');
	Route::post('/users/delete', 'Users@delete');
	Route::post('/users/newPassword/{id}', 'Users@newPassword');
	Route::post('/users/updatefoto/{id}', 'Users@edit_foto');
	Route::post('/users/getleader', 'Users@getleader');

	Route::get('/kegiatan/get', 'Kegiatan@get');
	Route::post('/kegiatan/getRow', 'Kegiatan@getRow');
	Route::post('/kegiatan/add', 'Kegiatan@add');
	Route::post('/kegiatan/upp/{id}', 'Kegiatan@update');
	Route::post('/kegiatan/delete', 'Kegiatan@delete');
	Route::post('/kegiatan/postfoto/{id}', 'Kegiatan@edit_foto');
// });
