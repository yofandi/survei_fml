<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersColumn2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('telephon_agt')->after('password');
            $table->string('tempat_lahir')->after('telephon_agt');
            $table->date('tanggal_lahir')->after('tempat_lahir');
            $table->text('alamat_agt')->after('tanggal_lahir');
            $table->string('foto_agt',200)->after('alamat_agt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('telephon_agt');
            $table->dropColumn('tempat_lahir');
            $table->dropColumn('tanggal_lahir');
            $table->dropColumn('alamat_agt');
            $table->dropColumn('foto_agt');
        });
    }
}
