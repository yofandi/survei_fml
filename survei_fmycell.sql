-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 01, 2019 at 08:26 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `survei_fmycell`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_10_20_162020_addcolumn_users', 2),
(6, '2019_08_19_000000_create_failed_jobs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('yofandirikiwinata24@gmail.com', '$2y$10$8NwZcKUWAQwU4xXHL0vLvuwcsE5QbU5jE00vpuuNUl6mVB89R/IU6', '2019-10-08 11:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `tb_familycell`
--

CREATE TABLE `tb_familycell` (
  `id` int(11) NOT NULL,
  `nama_cell` varchar(45) NOT NULL,
  `alamat_cell` varchar(45) NOT NULL,
  `ketua` int(20) DEFAULT NULL,
  `telephon` varchar(20) NOT NULL,
  `foto_familycell` varchar(200) DEFAULT NULL,
  `tb_wilayah_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_familycell`
--

INSERT INTO `tb_familycell` (`id`, `nama_cell`, `alamat_cell`, `ketua`, `telephon`, `foto_familycell`, `tb_wilayah_id`, `created_at`, `updated_at`) VALUES
(1, 'Brave', 'akd', 3, '389183', NULL, 1, '2019-10-11 21:26:26', '2019-10-12 02:14:37'),
(2, 'Shine', 'sdeb', NULL, '238919', NULL, 2, '2019-10-11 21:26:43', '2019-10-11 21:26:43'),
(3, 'Creatifully', 'kenk', NULL, '193981973', NULL, 3, '2019-10-11 21:27:04', '2019-10-11 21:27:04'),
(4, 'Peace', 'cnja', NULL, '29873', NULL, 4, '2019-10-11 21:27:34', '2019-10-11 21:27:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', '2019-10-11 20:00:04', '2019-10-11 20:00:04'),
(2, 'LEADER', '2019-10-11 20:00:42', '2019-10-11 20:00:42'),
(3, 'MEMBER', '2019-10-11 20:00:52', '2019-10-11 20:00:52');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id` int(11) NOT NULL,
  `tb_familycell_id` int(11) NOT NULL,
  `tanggal_keg` date NOT NULL,
  `lokasi_keg` text NOT NULL,
  `jml_dewasa` int(11) NOT NULL,
  `jml_anak` int(11) NOT NULL,
  `jml_jemaatbaru` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `foto_kegiatan` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id`, `tb_familycell_id`, `tanggal_keg`, `lokasi_keg`, `jml_dewasa`, `jml_anak`, `jml_jemaatbaru`, `total`, `foto_kegiatan`, `created_at`, `updated_at`) VALUES
(1, 2, '2019-10-25', 'swdsd', 10, 15, 25, 50, 'familycell20191023044844.jpg', '2019-10-12 15:51:00', '2019-10-22 21:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wilayah`
--

CREATE TABLE `tb_wilayah` (
  `id` int(11) NOT NULL,
  `nama_wil` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_wilayah`
--

INSERT INTO `tb_wilayah` (`id`, `nama_wil`, `created_at`, `updated_at`) VALUES
(1, 'Sawo', '2019-10-11 21:24:13', '2019-10-11 21:24:13'),
(2, 'Lenmarc', '2019-10-11 21:24:19', '2019-10-11 21:24:19'),
(3, 'Rungkut', '2019-10-11 21:24:26', '2019-10-11 21:24:26'),
(4, 'Rich Palace', '2019-10-11 21:24:35', '2019-10-11 21:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `telephon_agt` varchar(20) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat_agt` text DEFAULT NULL,
  `foto_agt` varchar(200) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tb_familycell_id_fc` int(11) DEFAULT NULL,
  `tb_kategori_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `telephon_agt`, `tempat_lahir`, `tanggal_lahir`, `alamat_agt`, `foto_agt`, `remember_token`, `email`, `password`, `email_verified_at`, `created_at`, `updated_at`, `tb_familycell_id_fc`, `tb_kategori_id`) VALUES
(1, 'yofandi', NULL, NULL, NULL, NULL, NULL, 'ggJgOwMNQVLEvVmlBkqg1DAwtd6BDf3bEBGxoXcWqCeAeZpqTvQwDJkUuA0s', 'yofandirikiwinata24@gmail.com', '$2y$10$Y.P3Z7OlR3FGo2g7cznUoO4wrKRFMhCmQ/sBsxQKBvDUcCi/nOWuS', NULL, '2019-10-08 11:07:14', '2019-10-25 13:43:46', NULL, 1),
(2, 'Yosia', '182681', 'Lumajang', '2019-10-22', 'dw', NULL, NULL, 'yosia@gmail.com', '$2y$10$sVeAw6lGCrplFb5MXpsiUei2/YyRZhOIo9nJ5OIiW/b.E5yPonAl2', NULL, '2019-10-11 17:00:00', '2019-10-22 02:31:09', 1, 3),
(3, 'Noril', '923082', 'Nias', '2019-10-28', 'sds', NULL, NULL, 'noril@gmail.com', '$2y$10$Q9LPYYnyQDYP4XsJy0lUke0sgZ.hNv9QLJ3p3YGAoVSvOXu10hvsq', NULL, '2019-10-12 01:53:07', '2019-10-12 02:14:36', 1, 2),
(4, 'yohana', '230892', 'none', '2019-10-26', 'dsmkns', NULL, NULL, 'yohana@gmail.com', '$2y$10$MYggybAqDrzigN6ZwZHG7.qbW4Wxw8P0Frr8RUxF.snpqek/h1iaq', NULL, '2019-10-22 02:35:32', '2019-10-22 02:35:32', 1, 3),
(5, 'owen', '128310', 'one', '2019-10-11', 'sjsb', NULL, NULL, 'owen@gmail.com', '$2y$10$Gux0uxdt1/Wp/1zJDadX0O2qNiClhuz8NULJ3f1unydzViSvOr.KS', NULL, '2019-10-22 10:38:46', '2019-10-22 10:38:46', 1, 3),
(6, 'Yohana', '93028983', 'none', '2019-10-11', 'jdskn', 'familycell20191022183301.jpg', NULL, 'yohana@gmail.com', '$2y$10$jdx0FLOfCt8TrbOD.YcdmudrhU3WOZKHPt9MrYJS1znJGqWF0dZ2C', NULL, '2019-10-22 10:44:32', '2019-10-22 11:33:01', 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tb_familycell`
--
ALTER TABLE `tb_familycell`
  ADD PRIMARY KEY (`id`,`tb_wilayah_id`),
  ADD KEY `fk_tb_familycell_tb_wilayah1_idx` (`tb_wilayah_id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id`,`tb_familycell_id`),
  ADD KEY `fk_tb_kegiatan_tb_familycell1_idx` (`tb_familycell_id`);

--
-- Indexes for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_tb_familycell1_idx` (`tb_familycell_id_fc`),
  ADD KEY `fk_users_tb_kategori1_idx` (`tb_kategori_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_familycell`
--
ALTER TABLE `tb_familycell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_familycell`
--
ALTER TABLE `tb_familycell`
  ADD CONSTRAINT `fk_tb_familycell_tb_wilayah1` FOREIGN KEY (`tb_wilayah_id`) REFERENCES `tb_wilayah` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD CONSTRAINT `fk_tb_kegiatan_tb_familycell1` FOREIGN KEY (`tb_familycell_id`) REFERENCES `tb_familycell` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_tb_familycell1` FOREIGN KEY (`tb_familycell_id_fc`) REFERENCES `tb_familycell` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_tb_kategori1` FOREIGN KEY (`tb_kategori_id`) REFERENCES `tb_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
